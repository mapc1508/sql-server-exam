select DateOfBirth, EmployeeFirstName, EmployeeMiddleName, EmployeeLastName
from tblEmployee
where DateOfBirth > '1971-12-24'

create nonclustered index idx_EmployeeNumber on tblEmployee(DateOfBirth) include (EmployeeFirstName, EmployeeMiddleName, EmployeeLastName)
-- much more efficient to use include to list fields that are often needed but are not used as key column used for searching
	-- they are stored only at the leaf level, where the actual data is stored

-- order of columns used as the main part of the index is important
	-- first things in the list are better optimized