declare @companyName nvarchar(255) = 'Shipper Test Merge', @phone nvarchar(255) = '(415) 555-0137';

MERGE INTO Sales.Shippers as tgt
USING (values (@companyName, @phone)) as src(name, phone) -- we need table result set as input to merge statement; way to display set of scalar data as a table result;
	ON tgt.companyName = src.name -- conditional used for comparing two sets
WHEN MATCHED THEN UPDATE -- updating when target exists
	SET tgt.companyName = src.name, tgt.phone = src.phone
WHEN NOT MATCHED THEN INSERT (companyName, phone) -- inserting when in target row does not exist
	 VALUES (src.name, src.phone);

-- 1 row affected

-- if we run this query once more we get the same output message: 1 row affected (update performed)
-- if we have same source and destination values update is still going to happen, could save resources needed for update

MERGE INTO Sales.Shippers as tgt
USING (values (@companyName, @phone)) as src(name, phone) 
	ON tgt.companyName = src.name 
WHEN MATCHED AND tgt.phone <> src.phone THEN UPDATE -- can add additional conditionals besides MATCHED/NOT MATCHED
	SET tgt.companyName = src.name, tgt.phone = src.phone
WHEN NOT MATCHED THEN INSERT (companyName, phone)
	 VALUES (src.name, src.phone);

-- 0 rows affected

/* PREVENTING LOCKS */
GO

declare @companyName nvarchar(255) = 'Shipper Test Merge', @phone nvarchar(255) = '(415) 555-0137';

-- with(holdlock/serializable) table hint - prevents merge conflicts in cases where two merge statements are executed at the same time on same rows
MERGE INTO Sales.Shippers WITH(HOLDLOCK) as tgt 
USING (values (@companyName, @phone)) as src(name, phone) 
	ON tgt.companyName = src.name 
WHEN MATCHED AND tgt.phone <> src.phone THEN UPDATE
	SET tgt.companyName = src.name, tgt.phone = src.phone
WHEN NOT MATCHED THEN INSERT (companyName, phone)
	 VALUES (src.name, src.phone);


/* WHEN NOT MATCHED BY SOURCE*/
GO

declare @companyName nvarchar(255) = 'Shipper Test Merge', @phone nvarchar(255) = '(415) 555-0137';

-- there is additional non-standard merge conditional that can check if the source does not contain target row
-- in those cases we can perform UPDATE/DELETE of the target rows

MERGE INTO Sales.Shippers WITH(HOLDLOCK) as tgt 
USING (values (@companyName, @phone)) as src(name, phone) 
	ON tgt.companyName = src.name 
WHEN MATCHED AND tgt.phone <> src.phone THEN UPDATE
	SET tgt.companyName = src.name, tgt.phone = src.phone
WHEN NOT MATCHED THEN INSERT (companyName, phone)
	 VALUES (src.name, src.phone)
WHEN NOT MATCHED BY SOURCE THEN DELETE;







