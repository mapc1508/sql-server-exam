select orderid, custid, empid, orderdate
from Sales.Orders
where orderid = 10248

select orderid, custid, empid, orderdate
from Sales.Orders
where orderid = 10249

select orderid, custid, empid, orderdate
from Sales.Orders
where orderid = 10250.0

/* Query shows that there are two parameterized cached plans, one where predicate is of type smallint (used 2x) and one where predicate is decimal number */
select qs.execution_count, qt.text
from sys.dm_exec_query_stats as qs -- queryStats
	 cross apply sys.dm_exec_sql_text(qs.sql_handle) as qt -- queryText
where 
	qt.text like '%Orders%'
	and qt.text not like '%qs.execution_count%'
order by qs.execution_count

select orderid, custid, empid, orderdate -- 1 row
from Sales.Orders
where custid = 13

select orderid, custid, empid, orderdate -- 2 rows
from Sales.Orders
where custid = 33

select orderid, custid, empid, orderdate -- 31 rows
from Sales.Orders
where custid = 71 

/* Query showing execution counts returns 3 different cached plans, optimizer does not parametrize the queries, not sure about the cardinality of the custid column */


/* Expecting the cached plan to be reused */

select orderid, custid, empid, orderdate
from Sales.Orders
where orderid = 10248

set concat_null_yields_null off;

/* Plan not reused, optimizer is conservative about changes made to server options */

select orderid, custid, empid, orderdate
from Sales.Orders
where orderid = 10249

set concat_null_yields_null off;


/* Using parametrized dynamic SQL */

declare @sql nvarchar(500) = '
	select orderid, custid, empid, orderdate
	from Sales.Orders
	where orderid = @orderId
'

declare @parameter nvarchar(500) = '@orderId INT';
declare @value int;

set @value = 10248

exec sp_executesql @sql, @parameter, @orderId = @value

set @value = 10249.0

exec sp_executesql @sql, @parameter, @orderId = @value

/* Even though the value passed is not of same type, by explicitly setting type of parameter (@parameter) the optimizer knows that it can reuse the cached plan for this query */

select orderid, custid, empid, orderdate
from Sales.Orders
where custid = 13 -- index seek + key lookup (1 row returned)

select orderid, custid, empid, orderdate
from Sales.Orders
where custid = 71 -- Clustered index scan (31 row returned)


go
create procedure Sales.GetCustomerOrders
(@custid int)
as
select orderid, custid, empid, orderdate
from Sales.Orders
where custid = @custid

dbcc freeproccache -- clearing the cache

exec Sales.GetCustomerOrders @custid = 13 
exec Sales.GetCustomerOrders @custid = 71 -- index seek + key lookup, unlike when used outside the SP

go
alter procedure Sales.GetCustomerOrders
(@custid int)
with recompile -- forcing the reexecution of plan each time SP is executed
as
select orderid, custid, empid, orderdate
from Sales.Orders
where custid = @custid

exec Sales.GetCustomerOrders @custid = 71 -- Clustered index scan

