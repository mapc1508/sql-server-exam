GO
ALTER PROCEDURE dbo.ParentStoredProcedure 

AS
BEGIN
	create table #temporaryData (id int primary key, [value] nvarchar(255))

	insert into #temporaryData (id, value)
	values (1, 'test'), (2, 'test1')

	exec ChildStoredProcedure
END
GO

GO
ALTER PROCEDURE dbo.ChildStoredProcedure 
AS
BEGIN
	select * from #temporaryData -- can use temp table provided by the parent stored procedure
END	
GO