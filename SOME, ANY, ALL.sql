declare @table table(id int)

INSERT @table VALUES (2) ;  
INSERT @table VALUES (2) ;  
INSERT @table VALUES (6) ;  
INSERT @table VALUES (2) ;  

select *
from (values (2), (6), (7), (8)) as x(id)
where x.id <> ALL(select id from @table)