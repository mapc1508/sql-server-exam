/* OPENXML function */

declare @handle int -- first element needed by openxml function
declare @xmlDocument as nvarchar(max);  -- actual xml data

set @xmlDocument= N'
	<CustomersOrders>
		<Customer custid="1">
			<companyname>Customer NRZBB</companyname>
			<Order orderid="10692">
				<orderdate>2007-10-03T00:00:00</orderdate>
			</Order>
			<Order orderid="10702">
				<orderdate>2007-10-13T00:00:00</orderdate>
			</Order>
			<Order orderid="10952">
				<orderdate>2008-03-16T00:00:00</orderdate>
			</Order>
		</Customer>
		<Customer custid="2">
			<companyname>Customer MLTDN</companyname>
			<Order orderid="10308">
				<orderdate>2006-09-18T00:00:00</orderdate>
			</Order>
			<Order orderid="10926">
				<orderdate>2008-03-04T00:00:00</orderdate>
			</Order>
		</Customer>
	</CustomersOrders>
'

EXEC sys.sp_xml_preparedocument @handle OUTPUT, @xmlDocument; -- prepares the xml document for shredding (creates dom representation of the XML file)

select *
from openxml (
	@handle,
	'/CustomersOrders/Customer', -- xPath to nodes that want to map to rows
	1 -- flag - type of mapping: 1 - attribute centric, 2 - element-centric, 11 - both (flag 8 + 1 + 2)
)
with (
	custid int,
	companyname nvarchar(255)
) -- how to map

select *
from openxml (
	@handle,
	'/CustomersOrders/Customer', -- xPath to nodes that want to map to rows
	2 -- flag - type of mapping: 1 - attribute centric, 2 - element-centric, 11 - both (flag 8 + 1 + 2)
)
with (
	custid int,
	companyname nvarchar(255)
) -- how to map

select *
from openxml (
	@handle,
	'/CustomersOrders/Customer', -- xPath to nodes that want to map to rows
	11 -- flag - type of mapping: 1 - attribute centric, 2 - element-centric, 11 - both (flag 8 + 1 + 2)
)
with (
	custid int,
	companyname nvarchar(255)
) -- how to map

-- remove the DOM
EXEC sys.sp_xml_removedocument @DocHandle;