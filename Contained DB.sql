use master

GO
sp_configure 'contained database authentication', 1
reconfigure

CREATE DATABASE [testContainedDB]
 CONTAINMENT = PARTIAL

GO
USE [testContainedDB]
GO
CREATE USER [testUser] WITHOUT LOGIN WITH DEFAULT_SCHEMA = [dbo] -- user without login is not bound to the server logins, can be used on any server without the need for additional user account configurations
GO
