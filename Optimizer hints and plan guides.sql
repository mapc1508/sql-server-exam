select qty, COUNT(*)
from Sales.OrderDetails
group by qty

select qty, COUNT(*)
from Sales.OrderDetails
group by qty
option (order group) -- forcing stream aggregate 

select orderid, productid, qty
from Sales.OrderDetails
where productid between 10 and 30
order by productid;

select orderid, productid, qty
from Sales.OrderDetails with (INDEX(idx_nc_productid)) -- forcing optimizer to use specific access method (specific index in this case)
where productid between 10 and 30
order by productid;

select o.custid, o.orderdate, od.orderid, od.productid, od.qty
from Sales.Orders o
	 inner join Sales.OrderDetails od on o.orderid = od.orderid
where o.orderid < 10250; -- query optimizer decides to use nested loops as a joining algorithm

select o.custid, o.orderdate, od.orderid, od.productid, od.qty
from Sales.Orders o
	 inner merge join Sales.OrderDetails od on o.orderid = od.orderid -- forcing optimizer to use the merge join algorithm
where o.orderid < 10250;

go
create procedure Sales.GetCustomerOrders
(@custid int)
as
select orderid, custid, empid, orderdate
from Sales.Orders
where custid = @custid

/* Creating plan guide to optimize SP for custid 71 (customer that has bigger amount of orders) - goal: use clustered index scan instead of seek + lookup */
exec sys.sp_create_plan_guide
	@name = 'Cust71', -- name of the plan
	@stmt = N'
		select orderid, custid, empid, orderdate
		from Sales.Orders
		where custid = @custid
	', -- SQL statement
	@type = 'OBJECT',
	@module_or_batch = N'Sales.GetCustomerOrders', -- creating plan guide for specific SP
	@params = NULL,
	@hints = N'OPTION (OPTIMIZE FOR (@custid = 71))'


exec Sales.GetCustomerOrders @custid = 71 -- clustered index scan

select *
from sys.plan_guides -- list of plan guides

exec sys.sp_control_plan_guide 'DROP', 'Cust71' -- dropping plan guide
drop procedure Sales.GetCustomerOrders

go
create procedure Sales.GetCustomerOrders
(@custid int)
as
begin
	select orderid, custid, empid, orderdate
	from Sales.Orders
	where custid = @custid
	option (recompile) -- cached plan recompiled on the single sql statement instead of whole procedure
end


exec Sales.GetCustomerOrders @custid = 13 -- index seek + lookup
exec Sales.GetCustomerOrders @custid = 71 -- index scan


