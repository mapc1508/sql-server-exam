select OBJECT_NAME(s.object_id) as table_name, i.name as index_name, s.user_seeks, s.user_scans, s.user_lookups
from sys.dm_db_index_usage_stats s
	 inner join sys.indexes i on s.object_id = i.object_id and s.index_id = i.index_id
where s.object_id = OBJECT_ID('Sales.Orders', 'U') -- index usage

select shipregion, count(*) as num_regions
from Sales.Orders
group by shipregion -- Table SCAN

select shipregion
from Sales.Orders
order by shipregion -- Table SCAN

create nonclustered index idx_nc_shipregion on Sales.Orders(shipregion)

select shipregion, count(*) as num_regions
from Sales.Orders
group by shipregion -- stream aggregate

select shipregion
from Sales.Orders
order by shipregion -- Index SCAN

create nonclustered index idx_nc_shipcity on Sales.Orders(shipcity)

select orderid, custid, shipcity
from Sales.Orders
where shipcity = N'Vancouver' -- idx_nc_shipcity - seek

select OBJECT_NAME(s.object_id) as table_name, i.name as index_name, s.user_seeks, s.user_scans, s.user_lookups
from sys.dm_db_index_usage_stats s
	 inner join sys.indexes i on s.object_id = i.object_id and s.index_id = i.index_id
where s.object_id = OBJECT_ID('Sales.Orders', 'U') -- index usage

select orderid, custid, shipcity
from Sales.Orders
where custid = 42 -- idx_nc_custid - seek

select orderid, custid, shipcity
from Sales.Orders
where custid = 42  or shipcity = N'Vancouver' -- Index scan due to inclusive or operator - num of rows expected: 6

select orderid, custid, shipcity
from Sales.Orders
where custid = 42  AND shipcity = N'Vancouver' -- Index seek when using AND operator


drop index idx_nc_shipcity on Sales.Orders

create nonclustered index idx_nc_shipcity on Sales.Orders(shipcity) include (custid) -- using included columns

select orderid, custid, shipcity
from Sales.Orders
where custid = 42  AND shipcity = N'Vancouver' -- Index seek, but this time without additional key lookup (custid stored at leaf levels - include statement)





