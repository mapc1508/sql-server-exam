/* Getting index information about Sales.Orders table */
SELECT
    name AS Index_Name,
    type_desc  As Index_Type,
    is_unique,
    OBJECT_NAME(object_id) As Table_Name
FROM
    sys.indexes
WHERE
    is_hypothetical = 0 AND
    index_id != 0 AND
    object_id = OBJECT_ID('Sales.Orders'); 

/* Query optimizer decided to sort the input and perform clustered index scan on Sales.Orders table instead of using non-clustered index scan and key lookup into the orders table  */
/* Reason: orderdate is not indexed */
select c.custid, c.companyname, c.address, c.city, o.orderdate, o.orderid
from Sales.Customers as c
	 inner join Sales.Orders as o on o.custid = c.custid 

/* When orderdate is ommited from the qury, the optimizer uses non-clustered index scan on Orders table */
select c.custid, c.companyname, c.address, c.city
from Sales.Customers as c
	 inner join Sales.Orders as o on o.custid = c.custid 

/* Query is very selective - optimizer uses key lookups to Sales.Orders table */
select c.custid, c.companyname, c.address, c.city, o.orderdate, o.orderid
from Sales.Customers as c
	 inner join Sales.Orders as o on o.custid = c.custid 
where c.city = N'Berlin'


/* City aggregation column is index - optimizer decides to use stream aggregation iterator */
select c.city, min(o.orderid) as minorderid
from Sales.Customers as c
	 inner join Sales.Orders as o on o.custid = c.custid 
group by c.city

