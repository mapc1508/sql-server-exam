/* difference between COALSESCE() and ISNULL() */

declare @test nvarchar(3) = NULL
declare @test2 nvarchar(255) = 'test2'
declare @test3 nvarchar(255) = 'test3'

/* num of arguments */
select COALESCE(@test, @test2, @test3, '') -- can have multiple arguments
select ISNULL(@test, '') -- only 2 arguments allowed

/* returnded type */
select COALESCE(@test, @test2) -- returns 'test2' - returned type defined by output value
select ISNULL(@test, @test2) -- returns 'tes' - returned type defined by first parameter nvarchar(3)


/* creating table using select into clause */
create table testCoalesceIsnull 
(
	test nvarchar(255) NULL -- column allows nulls
)

select ISNULL(test, '') as test into testCoalesceIsnull2 from testCoalesceIsnull -- creates test column and defines that it is NOT NULL
select COALESCE(test, '') as test into testCoalesceIsnull3 from testCoalesceIsnull -- creates test column and defines that it is NULL

/* NULLIF */
GO

declare @test1 int = 1
declare @test2 int = 2

select NULLIF(@test1, @test2) -- if not the same, first one returned

set @test2 = 1

select NULLIF(@test1, @test2) -- if same, NULL is returned

/* IIF */
GO

declare @test1 int = 1
declare @test2 int = 2

select IIF(@test1 = @test2, 'They are the same.', 'They are not the same.'); -- takes boolean input and returns second argument if it evaluates to true, or second if evaluates to false

/* CHOOSE */
GO

select CHOOSE(2, 'test1', 'test2', 'test3') -- test2 returned - returned the value by index defined as the first parameter


