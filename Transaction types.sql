/* IMPLICIT TRANSACTION TYPE */

SET IMPLICIT_TRANSACTIONS ON; -- don't require begin tran statement, every DML and DDL statement will trigger transaction that will last until it was commited/rolled back

SELECT @@TRANCOUNT; -- transaction level = 0

SET IDENTITY_INSERT Production.Products ON;

INSERT INTO Production.Products(productid, productname, supplierid, categoryid,unitprice, discontinued)
VALUES(150, N'Test2: Bad categoryid', 1, 1, 18.00, 0);

SELECT @@TRANCOUNT; -- 1 - in transcation

COMMIT TRAN;

SET IDENTITY_INSERT Production.Products OFF;

SET IMPLICIT_TRANSACTIONS OFF;

-- Remove the inserted row
DELETE FROM Production.Products WHERE productid = 101; -- Note the row is deleted

GO

/* EXPLICIT TRANSACTION TYPE */

SELECT @@TRANCOUNT; -- 0

BEGIN TRAN;

	SELECT @@TRANCOUNT; -- 1

	SET IDENTITY_INSERT Production.Products ON;

	INSERT INTO Production.Products(productid, productname, supplierid, categoryid, unitprice, discontinued)
	VALUES(101, N'Test2: Bad categoryid', 1, 1, 18.00, 0);

	SELECT @@TRANCOUNT; -- 1

	SET IDENTITY_INSERT Production.Products OFF;

COMMIT TRAN;

-- Remove the inserted row
DELETE FROM Production.Products WHERE productid = 101; -- Note the row is deleted

GO

/* NESTED TRANSACTIONS */

SELECT @@TRANCOUNT; -- = 0

BEGIN TRAN;
	SELECT @@TRANCOUNT; -- = 1
	
	BEGIN TRAN;
		SELECT @@TRANCOUNT; -- = 2
		
		-- Issue data modification or DDL commands here
			SET IDENTITY_INSERT Production.Products ON;

			INSERT INTO Production.Products(productid, productname, supplierid, categoryid, unitprice, discontinued)
			VALUES(101, N'Test2: Bad categoryid', 1, 1, 18.00, 0);

			SET IDENTITY_INSERT Production.Products OFF;

	COMMIT TRAN;

	SELECT @@TRANCOUNT; -- = 1
	
COMMIT TRAN;

SELECT @@TRANCOUNT; -- = 0