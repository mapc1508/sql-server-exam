GO

create procedure DeleteJobCandidate 
	@custId int
as
begin
	delete from Sales.Customers where custid = @custId
end


GO

DECLARE @ErrorVar INT; 
DECLARE @RowCountVar INT; 

EXEC DeleteJobCandidate @custId = 1

IF (@ErrorVar <> 0)

PRINT N'Error = ' + CAST(@ErrorVar AS NVARCHAR(8)) + N', Rows Deleted = ' + CAST(@RowCountVar AS NVARCHAR(8));