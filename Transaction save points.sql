BEGIN TRAN

	insert into Sales.Customers (companyname, contactname, contacttitle, address, city, region, postalcode, country, phone, fax)
	select 'Customer TEST',	'Jaffe, David',	'Sales Representative',	'Fauntleroy Circus 4567',	'London',	NULL,	'10064',	'UK',	'(171) 789-0123',	NULL

	SAVE TRAN Savepoint1 -- Creating a save point before going further

	insert into Sales.Customers (companyname, contactname, contacttitle, address, city, region, postalcode, country, phone, fax)
	select 'Customer TEST 1',	'Jaffe, David',	'Sales Representative',	'Fauntleroy Circus 4567',	'London',	NULL,	'10064',	'UK',	'(171) 789-0123',	NULL

	ROLLBACK TRAN Savepoint1 -- rolling back transaction to previous save point, last insert not executed

COMMIT TRAN