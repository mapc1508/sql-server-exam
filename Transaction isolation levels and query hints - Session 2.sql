/* read committed */ 

set transaction isolation level read committed; -- default one

begin tran
	
	/* trying to read data being changed in another transaciton */
	select * from Sales.Customers where custid = 14 -- blocked

	-- when the other transaction finishes this statement will go through

commit

/* read uncommitted */

set transaction isolation level read uncommitted;

begin tran
	
	/* trying to read data being changed in another transaciton */
	select * from Sales.Customers where custid = 14 -- not blocked

	-- even though the other transaction is still in progress reading uncommitted data is possible

commit

/* read commited as query hint */

set transaction isolation level read committed;

begin tran
	
	/* trying to read data being changed in another transaciton */
	select *
	from Sales.Customers with (readuncommitted) -- even though the other transaction is still in progress reading uncommitted data is possible using the query hint (not set on entire session)
	where custid = 14

	select * from Sales.Customers where custid = 14 -- blocked

commit