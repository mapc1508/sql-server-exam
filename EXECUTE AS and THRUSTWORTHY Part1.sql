create database testDatabase

alter database testDatabase set TRUSTWORTHY ON;

GO
alter procedure getData
as
begin

	select 1, 'test'

end

GRANT EXEC ON dbo.getData TO testlogin; -- testLogin belongs to dataReader database role

GO
alter procedure createTable
as
begin

	create table testTable (
		id int identity,
		test nvarchar(255)
	)

	insert into testTable select 'test'

	select * from testTable

	drop table testTable

end

GRANT EXEC ON dbo.createTable TO testlogin; 

GO
alter procedure createTable
with execute as owner
as
begin

	create table testTable (
		id int identity,
		test nvarchar(255)
	)

	insert into testTable select 'test'

	select * from testTable

	drop table testTable

end

GO
use testDatabase;

GO
alter procedure viewSystemLogs 
with execute as owner
as 
begin
	SELECT * FROM sys.dm_exec_requests;  	
end

GRANT EXEC ON dbo.createTable TO testlogin; 

use master;
GRANT VIEW SERVER STATE TO testOwner; -- user still does not have the rights to execute SP returning DM view

GO
use testDatabase;

ALTER ROLE [db_owner] ADD MEMBER [testlogin] -- adding testLogin to db_owner database role; now the testLogin is able to execute the SP
-- even though execute as owner was specified and the owner has VIEW SERVER STATE permissions and can execute the SP himself, testLogin can only execute it if he's in db_owner role, as well



