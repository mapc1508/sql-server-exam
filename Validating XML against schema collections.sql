create table Beverages (
	name nvarchar(255),
	type nvarchar(255),
	price decimal (18,2)
)

declare @xmlSchema nvarchar(max)

set @xmlSchema = (
	select *
	from Beverages
	where 1 = 0
	for xml auto, elements, xmlschema('BeveragesSchema')
)

create xml schema collection BeveragesSchemaCollection as @xmlSchema -- creating XML schema using xml defined in nvarchar(max) variable

declare @x xml(BeveragesSchemaCollection) 

set @x = 'test';

