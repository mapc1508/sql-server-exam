/* USING RAISERROR */

RAISERROR (
	'Error occured', -- MESSAGE
	16, -- LEVEL (0-25)
	0 -- STATE
);

/* can use string variable to hold the message */
declare @message nvarchar(255) = N'Error occured in %s database'

-- using FORMATMESSAGE to easily replace tags with values
set @message = FORMATMESSAGE(@message, N'TSQL2012')

RAISERROR (
	@message,
	16, -- LEVEL (0-25)
	0 -- STATE
);

RAISERROR (
	@message,
	16, -- LEVEL (0-25)
	0 -- STATE
)
with nowait; -- can add this option to send messages to caller immediately (not waiting till the end of batch to display it)
GO

/* USING THROW STATEMENT */

-- no parenthesis
-- no severity level (always 16)
throw 
	5000, -- error number
	'Error occured in TSQL2012 database', -- message
	0 -- state

begin try
	print 'test'
end try

begin catch
	throw; --only in catch blok throw can be executed without parameters
end catch

GO

select TRY_CONVERT(datetime, '06-27-2019') -- returns 2019-06-27 00:00:00.000
select TRY_CONVERT(datetime, '06-xx-2019') -- returns NULL

select TRY_PARSE('06-27-2019' as datetime) -- returns 2019-06-27 00:00:00.000
select TRY_PARSE('06-xx-2019' as datetime) -- returns NULL

GO

-- unstructured error handling

set xact_abort on; -- any error encountered automatically rollbacks transaction and stops further batch processing

GO

create procedure test as
begin

	select CONVERT(datetime, '06-xx-2019')

	select @@ERROR -- returns 0 if good, 1 if error
	
end

GO

-- structured error handling

begin try
	select CONVERT(datetime, '06-xx-2019')
end try

begin catch
	print 'failed'

	/* function available that can give more info about the error */
	select 
		ERROR_LINE(),
		ERROR_NUMBER(),
		ERROR_MESSAGE(),
		ERROR_PROCEDURE(),
		ERROR_SEVERITY(),
		ERROR_STATE()
end catch







