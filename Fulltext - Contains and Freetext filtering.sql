-- Exact match
select * from Documents where contains(doccontent, 'warehouse')

-- Exact match with multiple conditions
select * from Documents where contains(doccontent, 'price AND NOT bleeding')

-- Searching for a phrase (exact match)
select * from Documents where contains(doccontent, '"The simplest type are measures"')

-- Word that starts with... (prefix)
select * from Documents where contains(doccontent, '"Dimension*"')

-- Searching for occurence of multiple words
select * from Documents where contains(doccontent, 'NEAR(across, dimensions)')

-- Adding second parameter to NEAR function: proximity (maximum num of words that separate the two words)
select * from Documents where contains(doccontent, 'NEAR((language, build), 4)') -- nothing returned
select * from Documents where contains(doccontent, 'NEAR((language, build), 5)') -- result returned

-- Adding third parameter to NEAR function: order - if set to 1 order of the words searched for matters
select * from Documents where contains(doccontent, 'NEAR((build, language), 5, true)')

-- Different forms of the word - FORMSOF function with INFLECTIONAL option; Text contains discount word, but search finds discounts
select * from Documents where contains(doccontent, 'FORMSOF(INFLECTIONAL, discounts)')

-- Searching includes synonims for the selected word (based on the THESAURUS file)
select * from Documents where contains(doccontent, 'FORMSOF(THESAURUS, discounts)')

-- Applying weights to search terms, results are ranked depending accordingly (ranking is not applied when using CONTAINS, should use CONTAINSTABLE instead)
-- Weights range: 0.0-1.0	
select * from Documents where contains(doccontent, 'ISABOUT(accross weight(0.2), dimensions weight(0.6))')


/* CREATING SEARCH PROPERTY LIST - for documents */

CREATE SEARCH PROPERTY LIST WordSearchPropertyList;
GO

ALTER SEARCH PROPERTY LIST WordSearchPropertyList
ADD 'Authors'
WITH 
	(
		PROPERTY_SET_GUID = 'F29F85E0-4FF9-1068-AB91-08002B27B3D9',
		PROPERTY_INT_ID = 4,
		PROPERTY_DESCRIPTION = 'System.Authors - authors of a given item.'
	);

ALTER FULLTEXT INDEX ON Documents SET SEARCH PROPERTY LIST WordSearchPropertyList; -- add search property list to index

-- Searching for specific property value in the document
SELECT * FROM Documents WHERE CONTAINS ( PROPERTY ( doccontent, 'Authors' ), 'Dejan') GO

/* FREETEXT SEARCH */

-- not as selective, for each word or phrase looks for synonyms and inflectional words
SELECT * FROM Documents WHERE FREETEXT(doccontent, 'across, dimensions') 


/* Semantic Search Functions */

ALTER FULLTEXT INDEX ON Documents alter column doccontent ADD STATISTICAL_SEMANTICS  -- adding STATISTICAL_SEMANTICS

-- returns key phrases associated to that document
select *
from SEMANTICKEYPHRASETABLE (Documents, doccontent, 1)
order by score desc

-- comparing similarity between 2 documents (keyphrases)
select *
from SEMANTICSIMILARITYDETAILSTABLE (
	Documents, -- table
	doccontent, -- indexed source column
	1, -- source document id
	doccontent, -- indexed matching column
	2 -- indexed matching document id
) 
order by score desc

-- searching for documents most similar to one specified by id
select *
from SEMANTICSIMILARITYTABLE
( 
	Documents, -- table
	doccontent, -- indexed column
	1 -- documentID
)
order by score desc


/* CONTAINSTABLE */

-- documents sorted by the usage of words data or level in them
select *
from CONTAINSTABLE(Documents, doccontent, N'data OR level') as ct
	 inner join Documents doc on doc.id = ct.[key]
order by ct.RANK desc


-- sorting documents by weighted terms
select * 
from Documents doc
	 inner join CONTAINSTABLE(Documents, doccontent, N'ISABOUT(accross weight(0.2), dimensions weight(0.6))') as ct on doc.id = ct.[key]
order by ct.rank desc


/* FREETEXTTABLE */

select *
from FREETEXTTABLE(Documents, doccontent, N'data') as ct
	 inner join Documents doc on doc.id = ct.[key]
order by ct.RANK desc
