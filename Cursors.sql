set nocount on;

declare @custID as int;

declare cust_cursor cursor fast_forward for
select custid
from Sales.Customers

open cust_cursor

fetch next from cust_cursor into @custID

while @@FETCH_STATUS = 0
begin
	
	print 'Current customer ID: ' + CAST(@custID as nvarchar(255));

	fetch next from cust_cursor into @custID

end

close cust_cursor;
deallocate cust_cursor;

/* Alternative approach to cursors for handling iterations */

declare @currentCustId int = (select top 1 custid from Sales.Customers order by custid asc)

while @currentCustId is not null
begin

	print 'Current customer ID: ' + CAST(@currentCustId as nvarchar(255));	

	set @currentCustId = (select top 1 custid from Sales.Customers where custid > @currentCustId order by custid asc)
end


/* RUNNING TOTAL USING SET BASED OPEARTION */
select SUM(val) OVER(partition by actid order by tranid rows unbounded preceding), *
from Transactions
