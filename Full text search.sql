SELECT SERVERPROPERTY('IsFullTextInstalled'); -- check if full text is installed

EXEC sys.sp_help_fulltext_system_components 'filter'; -- getting list of ifilters (they extract text from metada in the document)

SELECT lcid, name
FROM sys.fulltext_languages
ORDER BY name; -- list of fulltext languages supported


/* list of words that should be excluded from the search */
SELECT stoplist_id, name
FROM sys.fulltext_stoplists;
SELECT stoplist_id, stopword, language
FROM sys.fulltext_stopwords;

-- creating table for full text indexing

CREATE TABLE dbo.Documents
(
	id INT IDENTITY(1,1) NOT NULL,
	title NVARCHAR(100) NOT NULL,
	doctype NCHAR(4) NOT NULL,
	docexcerpt NVARCHAR(1000) NOT NULL,
	doccontent VARBINARY(MAX) NOT NULL,
	CONSTRAINT PK_Documents
	PRIMARY KEY CLUSTERED(id)
);

/* inserting documents into table */

INSERT INTO dbo.Documents
	(title, doctype, docexcerpt, doccontent)
SELECT 
	N'Columnstore Indices and Batch Processing',
	N'docx',
	N'You should use a columnstore index on your fact tables, putting all columns of a fact table in a columnstore index. In addition to fact tables, very large dimensions could benefit from columnstore indices as well. Do not use columnstore indices for small dimensions. ',
	bulkcolumn
FROM OPENROWSET(BULK 'C:\TK461\ColumnstoreIndicesAndBatchProcessing.docx',SINGLE_BLOB) AS doc;

INSERT INTO dbo.Documents
	(title, doctype, docexcerpt, doccontent)

SELECT 
	N'Introduction to Data Mining',
	N'docx',
	N'Using Data Mining is becoming more a necessity for every company and not an advantage of some rare companies anymore. ',
	bulkcolumn
FROM OPENROWSET(BULK 'C:\TK461\IntroductionToDataMining.docx', SINGLE_BLOB) AS doc;

INSERT INTO dbo.Documents
	(title, doctype, docexcerpt, doccontent)
SELECT 
	N'Why Is Bleeding Edge a Different Conference',
	N'docx',
	N'During high level presentations attendees encounter many questions. For the third year, we are continuing with the breakfast Q&A session. It is very popular, and for two years now, we could not accommodate enough time for all questions and discussions! ',
	bulkcolumn
FROM OPENROWSET(BULK 'C:\TK461\WhyIsBleedingEdgeADifferentConference.docx', SINGLE_BLOB) AS doc;

INSERT INTO dbo.Documents
	(title, doctype, docexcerpt, doccontent)
SELECT 
	N'Additivity of Measures',
	N'docx',
	N'Additivity of measures is not exactly a data warehouse design problem. However, you have to realize which aggregate functions you will use in reports for which measure, and which aggregate functions you will use when aggregating over which dimension.',
	bulkcolumn
FROM OPENROWSET(BULK 'C:\TK461\AdditivityOfMeasures.docx', SINGLE_BLOB) AS doc;

/* CREATING A STOPLIST */

create fulltext stoplist SQLStopList;

alter fulltext stoplist SQLStopList
add N'SQL' -- stop word
language 'English'; -- for which language

select 
	slist.name, 
	sword.stopword, 
	sword.language,
	slist.stoplist_id
from 
	sys.fulltext_stoplists slist
	inner join sys.fulltext_stopwords sword on slist.stoplist_id = sword.stoplist_id

/*
SQLStopList	SQL	English
*/

declare @text nvarchar(max) = N'
"Additivity of measures is not exactly a data warehouse design problem.
However, you have to realize which aggregate functions you will use
in reports for which measure, and which aggregate functions
you will use when aggregating over which dimension SQL."
'

-- displays how text is parsed by full text search
SELECT *
FROM sys.dm_fts_parser
(
	@text, 
	1033, -- language
	7, -- stoplist id
	0 -- accent sensitivity
); 


	SELECT *
	FROM sys.dm_fts_parser
	('FORMSOF(INFLECTIONAL,'+ 'function' + ')', 1033, 7, 0); -- different forms of word 'function' (stemmers)


/* CREATING FULLTEXT CATALOG */

create fulltext catalog DocumentsFulltextCatalog; -- virtual container for fulltext indexes

/* CREATING FULLTEXT INDEX */

create fulltext index on dbo.Documents
( 
	--list of columnds that need to be indexed
	docexcerpt language 1033,
	doccontent type column doctype language 1033 -- specify the type column for documents (varbinary)
)
key index PK_Documents -- unique index used
on DocumentsFulltextCatalog -- name of the catalog
with 
	-- additional properties: stoplist, document properties, change tracking...
	stoplist = SQLStopList,
	CHANGE_TRACKING AUTO
	--SEARCH PROPERTY LIST = WordSearchPropertyList




