/* GROUPING SETS */

select shipperid, YEAR(shippeddate) as [year], COUNT(*) as [total]
from Sales.Orders
group by GROUPING SETS (
	(shipperid, YEAR(shippeddate)), -- group by shipperid and year
	(shipperid), -- group by shipperid
	YEAR(shippeddate), -- group by shippeddate
	() -- grand total
)

-- NULL value means that column is not included in grouping in that row

/* CUBE */

GO

-- don't need to define grouping explicitly: it takes into account all possible grouping variations

select shipperid, YEAR(shippeddate) as [year], COUNT(*) as [total]
from Sales.Orders
group by CUBE (shipperid, YEAR(shippeddate))

-- groupings included: 
	--(shipperid, YEAR(shippeddate)), -- group by shipperid and year
	--(shipperid), -- group by shipperid
	--YEAR(shippeddate), -- group by shippeddate
	--() -- grand total

/* ROLLUP */

select shipcountry, shipregion, shipcity, COUNT(*) as [total]
from Sales.Orders
group by ROLLUP (shipcountry, shipregion, shipcity)


-- similar to cube but instead of calculating all possibilites for grouping, it considers hierarchy country => region => city
-- (country, region, city), (country, region), (country), ()

/* HOW TO DETERMINE IF NULL VALUE REPRESENTS NON-GROUPING ELEMENT OR ACTUAL NULL VALUE? */

/* GROUPING */

-- GROUPING returns 0 if column is part of grouping, 1 if it is not
select 
	shipperid, GROUPING(shipperid) as grp_shipperid,
	YEAR(shippeddate) as [year], GROUPING(YEAR(shippeddate)) as grp_shippeddate,
	COUNT(*) as [total]
from Sales.Orders
group by CUBE (shipperid, YEAR(shippeddate))

/* GROUPING_ID */

-- GROUPING_ID represents bitmap of grouped columns, it is a sum of column values where rightmost column is 2 to the power of (position num - 1) - 1,2,4,8...
select 
	GROUPING_ID(shipperid, YEAR(shippeddate)),
	shipperid, 
	YEAR(shippeddate) as [year],
	COUNT(*) as [total]
from Sales.Orders
group by CUBE (shipperid, YEAR(shippeddate))