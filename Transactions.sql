

begin transaction

	update tblEmployee set EmployeeNumber = 122 where EmployeeNumber = 123

	waitfor delay '00:00:10'

rollback tran

begin tran

	select * from dbo.tblEmployee -- read commited by default => waits until second tran is finished

commit tran


------------------------------------------------------

set transaction isolation level read uncommitted

begin tran

	select * from dbo.tblEmployee -- can read while other tran is updating, can cause dirty reads

commit tran

------------------------------------------------------


set transaction isolation level repeatable read

	-- cannot read modified but yet unocommited data
	-- no other transactions can modify data read by current transaction

begin tran

	select * from dbo.tblEmployee

	waitfor delay '00:00:20'

	select * from dbo.tblEmployee

commit tran



begin transaction

	update tblEmployee set EmployeeNumber = 122 where EmployeeNumber = 123 -- cannot update because the same table will be read multiple times before the transaction ends

rollback tran

begin transaction

	insert into tblEmployee (EmployeeNumber, EmployeeFirstName, EmployeeLastName, EmployeeGovernmentID) -- the insert statement can be executed while repeatable read transaction is executing
	select '1243423234', 'Mate', 'Matic', 'H'

	-- if the other transaction is using - set transaction isolation level serializable we will prevent new rows from being added while that transaction is reading data

commit transaction


------------------------------------------------------











