declare @someTable table (test nvarchar(max)) -- must be nvarchar(max), varchar(max) or varbinary(max) column

insert into @someTable select 'Lorem ipsum...'

update @someTable set test.WRITE (N'Adding some text', NULL, 0) -- .WRITE(text, offset, length) -- performance gains

select * from @someTable