/* FOR XML RAW */

with xmlnamespaces('TSQL2012' as ord) -- where the namespace is defined
select orderdate, custid, shippeddate, shipaddress as shipment_address -- if alias is specified it will be shown by that name in the xml
from Sales.Orders
where orderid < 10250
for xml 
	 raw('Order') -- if not specified returns (row) as element name foreach row
	,elements -- returns values as separate elements instead of attributes
	,root('Orders') -- defining root element

GO

/*
<Orders xmlns:ord="TSQL2012">
  <Order>
    <orderdate>2006-07-04T00:00:00</orderdate>
    <custid>85</custid>
    <shippeddate>2006-07-16T00:00:00</shippeddate>
    <shipment_address>6789 rue de l'Abbaye</shipment_address>
  </Order>
  <Order>
    <orderdate>2006-07-05T00:00:00</orderdate>
    <custid>79</custid>
    <shippeddate>2006-07-10T00:00:00</shippeddate>
    <shipment_address>Luisenstr. 9012</shipment_address>
  </Order>
</Orders>
*/


/* FOR XML AUTO */

with xmlnamespaces('TSQL2012' as ord)
select orderdate, custid, shippeddate, shipaddress
from Sales.Orders as [Order]
where orderid < 10250
for xml 
	 auto -- don't need to specify how each row element will be called like in raw option (aliases or schema.table names define the element)
	,elements -- returns values as separate elements instead of attributes
	,root('Orders')

/*
<Orders xmlns:ord="TSQL2012">
  <Order>
    <orderdate>2006-07-04T00:00:00</orderdate>
    <custid>85</custid>
    <shippeddate>2006-07-16T00:00:00</shippeddate>
    <shipaddress>6789 rue de l'Abbaye</shipaddress>
  </Order>
  <Order>
    <orderdate>2006-07-05T00:00:00</orderdate>
    <custid>79</custid>
    <shippeddate>2006-07-10T00:00:00</shippeddate>
    <shipaddress>Luisenstr. 9012</shipaddress>
  </Order>
</Orders>
*/

GO


select orderdate, custid, shippeddate, shipaddress
from Sales.Orders as [Order]
where 1=2 -- want to return only xmlschema without actual data
for xml 
	auto, 
	elements,
	xmlschema('TSQL2012-CustomerOrders') -- returns xsd file that contains definition of the returned xml object

/*
<xsd:schema targetNamespace="TSQL2012-CustomerOrders" xmlns:schema="TSQL2012-CustomerOrders" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:sqltypes="http://schemas.microsoft.com/sqlserver/2004/sqltypes" elementFormDefault="qualified">
  <xsd:import namespace="http://schemas.microsoft.com/sqlserver/2004/sqltypes" schemaLocation="http://schemas.microsoft.com/sqlserver/2004/sqltypes/sqltypes.xsd" />
  <xsd:element name="Order">
    <xsd:complexType>
      <xsd:sequence>
        <xsd:element name="orderdate" type="sqltypes:datetime" />
        <xsd:element name="custid" type="sqltypes:int" minOccurs="0" />
        <xsd:element name="shippeddate" type="sqltypes:datetime" minOccurs="0" />
        <xsd:element name="shipaddress">
          <xsd:simpleType>
            <xsd:restriction base="sqltypes:nvarchar" sqltypes:localeId="1033" sqltypes:sqlCompareOptions="IgnoreCase IgnoreKanaType IgnoreWidth" sqltypes:sqlSortId="52">
              <xsd:maxLength value="60" />
            </xsd:restriction>
          </xsd:simpleType>
        </xsd:element>
      </xsd:sequence>
    </xsd:complexType>
  </xsd:element>
</xsd:schema>
*/

GO

/* FOR XML PATH */

with xmlnamespaces('TSQL2012' as ord)
select 
	shipaddress as [@shipaddress] -- if want to have attribute centric display need to use (@) in the alias
	,orderdate, custid, shippeddate
from Sales.Orders as [Order]
where orderid < 10250
for xml path('Order'), -- where xPath is defined 
	root('Orders')

/*
<Orders xmlns:ord="TSQL2012">
  <Order shipaddress="6789 rue de l'Abbaye">
    <orderdate>2006-07-04T00:00:00</orderdate>
    <custid>85</custid>
    <shippeddate>2006-07-16T00:00:00</shippeddate>
  </Order>
  <Order shipaddress="Luisenstr. 9012">
    <orderdate>2006-07-05T00:00:00</orderdate>
    <custid>79</custid>
    <shippeddate>2006-07-10T00:00:00</shippeddate>
  </Order>
</Orders>
*/

select 
	 shippeddate [@ShipmentDate]
	,shipaddress as [Address]
	,orderdate [OrderDate]
	,( 
		-- need to define subquery in select list to return nested xml elements in for xml path
		select cust.companyname as [Name], cust.[address] as Address, cust.phone as PhoneNumber
		from Sales.Customers cust
		where custid = ord.custid
		for xml path('Customer'), type, root('Customers')
	)
from Sales.Orders as ord
where orderid < 10250
for xml path('Order'),
	root('Orders')

/*
<Orders>
  <Order ShipmentDate="2006-07-16T00:00:00">
    <Address>6789 rue de l'Abbaye</Address>
    <OrderDate>2006-07-04T00:00:00</OrderDate>
    <Customers>
      <Customer>
        <Name>Customer ENQZT</Name>
        <Address>5678 rue de l'Abbaye</Address>
        <PhoneNumber>56.78.90.12</PhoneNumber>
      </Customer>
    </Customers>
  </Order>
  <Order ShipmentDate="2006-07-10T00:00:00">
    <Address>Luisenstr. 9012</Address>
    <OrderDate>2006-07-05T00:00:00</OrderDate>
    <Customers>
      <Customer>
        <Name>Customer FAPSM</Name>
        <Address>Luisenstr. 0123</Address>
        <PhoneNumber>0251-456789</PhoneNumber>
      </Customer>
    </Customers>
  </Order>
</Orders>
*/

