exec getData -- current user belongs to db_reader database role. Has rights on SP. Since SP only returns data the user can execute it.

exec createTable -- user can only execute SP if the execute as owner is specified when SP was created

exec viewSystemLogs
-- Msg 229, Level 14, State 5, Procedure viewSystemLogs, Line 1 [Batch Start Line 0] The EXECUTE permission was denied on the object 'viewSystemLogs', database 'testDatabase', schema 'dbo'.
-- SP has execute as owner set; user still cannot execute the SP beacuse the SP returns data from DM view (server rights needed instead of database rights)

