DECLARE @x AS XML;
SET @x=N'
<root>
	<a>1<c>3</c><d>4</d></a>
	<b>2</b>
</root>';

select @x.query('*') -- returning whole sequence
/*
<root>
  <a>1<c>3</c><d>4</d></a>
  <b>2</b>
</root>
*/

select @x.query('data(*)') -- returning all data value sequence (data() is atomic)
/*
1342
*/

select @x.query('data(root/a/c)') -- returning value for a specific node 
/*
3
*/

-- every identifier in the xQuery expression is called QName = qualified name
GO

DECLARE @x AS XML;
SET @x='
<CustomersOrders xmlns:co="TK461-CustomersOrders">
	<co:Customer co:custid="1" co:companyname="Customer NRZBB">
		<co:Order co:orderid="10692" co:orderdate="2007-10-03T00:00:00" />
		<co:Order co:orderid="10702" co:orderdate="2007-10-13T00:00:00" />
		<co:Order co:orderid="10952" co:orderdate="2008-03-16T00:00:00" />
	</co:Customer>
	<co:Customer co:custid="2" co:companyname="Customer MLTDN">
		<co:Order co:orderid="10308" co:orderdate="2006-09-18T00:00:00" />
		<co:Order co:orderid="10926" co:orderdate="2008-03-04T00:00:00" />
	</co:Customer>
</CustomersOrders>';

/* XQuery Prolog, Namespace, Comments */


select @x.query('
	(: Define variables - prolog :)
	(: Explicit namespace :)
	declare namespace co="TK461-CustomersOrders";

	(: XPath :)
	//co:Customer[1]/*
')

/*
<co:Order xmlns:co="TK461-CustomersOrders" co:orderid="10692" co:orderdate="2007-10-03T00:00:00" />
<co:Order xmlns:co="TK461-CustomersOrders" co:orderid="10702" co:orderdate="2007-10-13T00:00:00" />
<co:Order xmlns:co="TK461-CustomersOrders" co:orderid="10952" co:orderdate="2008-03-16T00:00:00" />
*/

select @x.query('
	(: Define variables - prolog :)
	(: Default namespace :)
	declare default element namespace "TK461-CustomersOrders";

	(: XPath - since this is default namespace we cannot use co... :)
	//Customer[1]/*
')

/*
<Order xmlns="TK461-CustomersOrders" xmlns:p1="TK461-CustomersOrders" p1:orderid="10692" p1:orderdate="2007-10-03T00:00:00" />
<Order xmlns="TK461-CustomersOrders" xmlns:p2="TK461-CustomersOrders" p2:orderid="10702" p2:orderdate="2007-10-13T00:00:00" />
<Order xmlns="TK461-CustomersOrders" xmlns:p3="TK461-CustomersOrders" p3:orderid="10952" p3:orderdate="2008-03-16T00:00:00" />
*/

;with xmlnamespaces('TK461-CustomersOrders' as co)
select 
	@x.query('
	(: Namespace declared in TSQL using WITH statement :)

	(: XPath :)
	//co:Customer[1]/*
')

/*
<co:Order xmlns:co="TK461-CustomersOrders" co:orderid="10692" co:orderdate="2007-10-03T00:00:00" />
<co:Order xmlns:co="TK461-CustomersOrders" co:orderid="10702" co:orderdate="2007-10-13T00:00:00" />
<co:Order xmlns:co="TK461-CustomersOrders" co:orderid="10952" co:orderdate="2008-03-16T00:00:00" />
*/
GO

DECLARE @x AS XML;
SET @x='

<CustomersOrders>
	<Customer custid="1" companyname="Customer NRZBB">
		<Order orderid="10692" orderdate="2007-10-03T00:00:00" />
		<Order orderid="10702" orderdate="2007-10-13T00:00:00" />
		<Order orderid="10952" orderdate="2008-03-16T00:00:00" />
	</Customer>
	<Customer custid="2" companyname="Customer MLTDN">
		<Order orderid="10308" orderdate="2006-09-18T00:00:00" />
		<Order orderid="10926" orderdate="2008-03-04T00:00:00" />
	</Customer>
</CustomersOrders>';

select @x.query('
	(: $i represents one Customer :)
	(: iterations - FLWOR expression :)

	for $i in //Customer
	return 
		<OrdersInfo>
			{ $i/@companyname }
			
			<NumberOfOrders>
				{ count($i/Order) }	
			</NumberOfOrders>

			<LastOrder>
				{ max($i/Order/@orderid) }
			</LastOrder>
		</OrdersInfo>
')

/*
	<OrdersInfo companyname="Customer NRZBB">
	  <NumberOfOrders>3</NumberOfOrders>
	  <LastOrder>10952</LastOrder>
	</OrdersInfo>
	<OrdersInfo companyname="Customer MLTDN">
	  <NumberOfOrders>2</NumberOfOrders>
	  <LastOrder>10926</LastOrder>
	</OrdersInfo>
*/
GO

declare @x as xml = '
<!--Students grades are uploaded by months-->
<class_list>
   <student>
      <name>Tanmay</name>
      <grade>A</grade>
   </student>
</class_list>'

select @x.query('
	comment()
') -- extracts comments from the XML

/*
<!--Students grades are uploaded by months-->
*/

select @x.query('*') -- wildcard search - returns only principal nodes
/*
<class_list>
  <student>
    <name>Tanmay</name>
    <grade>A</grade>
  </student>
</class_list>
*/

select @x.query('node()') -- node() function returns all nodes
/*
<!--Students grades are uploaded by months-->
<class_list>
  <student>
    <name>Tanmay</name>
    <grade>A</grade>
  </student>
</class_list>
*/
GO

/* PREDICATES */

declare @x as xml = '
<breakfast_menu>
	<food>
		<name>Belgian Waffles</name>
		<price>$5.95</price>
		<description>
	   Two of our famous Belgian Waffles with plenty of real maple syrup
	   </description>
		<calories>650</calories>
	</food>
	<food>
		<name>Strawberry Belgian Waffles</name>
		<price>$7.95</price>
		<description>
		Light Belgian waffles covered with strawberries and whipped cream
		</description>
		<calories>900</calories>
	</food>
	<food>
		<name>Berry-Berry Belgian Waffles</name>
		<price>$8.95</price>
		<description>
		Belgian waffles covered with assorted fresh berries and whipped cream
		</description>
		<calories>900</calories>
	</food>
</breakfast_menu>'

/* Numeric predicates */
select @x.query('
	//food[1]
') -- selecting first element that matches path predicate

/*
<food>
  <name>Belgian Waffles</name>
  <price>$5.95</price>
  <description>
	   Two of our famous Belgian Waffles with plenty of real maple syrup
	   </description>
  <calories>650</calories>
</food>
*/

select @x.query('
	(//food/price)[1]
') -- selecting first group

/*
<price>$5.95</price>
*/

GO

DECLARE @x AS XML = N'
	<Employee empid="2">
		<FirstName>fname</FirstName>
		<LastName>lname</LastName>
	</Employee>
';

declare @v nvarchar(255) = 'FirstName'

select @x.query('
	(: sql:variable function retreives the value of a function :)

	if (sql:variable("@v") = "FirstName") then Employee/FirstName
	else Employee/LastName
') -- using if then else conditionals (similar to case statement in SQL)

/*
<FirstName>fname</FirstName>
*/

GO

/* FLOWR expressions */

DECLARE @x AS XML;
SET @x = N'
	<CustomersOrders>
		<Customer custid="1">
			<!-- Comment 111 -->
			<companyname>Customer NRZBB</companyname>
			<Order orderid="10692">
				<orderdate>2007-10-03T00:00:00</orderdate>
			</Order>
			<Order orderid="10702">
				<orderdate>2007-10-13T00:00:00</orderdate>
			</Order>
			<Order orderid="10952">
				<orderdate>2008-03-16T00:00:00</orderdate>
			</Order>
		</Customer>
		<Customer custid="2">
			<!-- Comment 222 -->
			<companyname>Customer MLTDN</companyname>
			<Order orderid="10308">
				<orderdate>2006-09-18T00:00:00</orderdate>
			</Order>
			<Order orderid="10952">
				<orderdate>2008-03-04T00:00:00</orderdate>
			</Order>
		</Customer>
	</CustomersOrders>';

select @x.query('
	for $i in //Customer (: Iterating through customers :)
	let $j := data($i/Order/orderdate) (: Assigning value to additional variable :)
	order by ($j)[1] (: Ordering by value - must be atomic, hence [1] :)
	return <Customer custid="{ $i/@custid }"> 
				<FirstOrder> { data($j[1]) } </FirstOrder> (: returning first order for the customer :)
			</Customer>
')

/*
<Customer custid="2">
  <FirstOrder>2006-09-18T00:00:00</FirstOrder>
</Customer>
<Customer custid="1">
  <FirstOrder>2007-10-03T00:00:00</FirstOrder>
</Customer>
*/