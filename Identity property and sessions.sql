insert into Sales.Shippers (companyname, phone)
select 'Shipper TEST', '(415) 555-0136'

select SCOPE_IDENTITY(); -- 4 - returns identity for current session and scope
select @@IDENTITY; -- 4 - returns identity for current session regardless of scope
select IDENT_CURRENT('Sales.Shippers'); -- 4 - returns identity value for the table, regardless of scope and session

GO
alter procedure testIdentityValues
as
begin
	insert into Sales.Shippers (companyname, phone)
	select 'Shipper TEST', '(415) 555-0136'

	exec testIdentityValuesInner

	select SCOPE_IDENTITY(); 
	select @@IDENTITY; 
	select IDENT_CURRENT('Sales.Shippers'); 
end

GO
alter procedure testIdentityValuesInner
as
begin
	insert into Sales.Shippers (companyname, phone)
	select 'Shipper TEST', '(415) 555-0136'
end

exec testIdentityValues

select SCOPE_IDENTITY(); -- 7: last in the current scope and session
select @@IDENTITY; -- 8: last in the current session regardless of scope (taking into account the call to other stored procedure)
select IDENT_CURRENT('Sales.Shippers'); -- 8: last for that table


