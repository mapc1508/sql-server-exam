CREATE VIEW GermanCustomers
AS
	select *
	from Sales.Customers
	where country = 'Germany'

-- basic syntax

GO
ALTER VIEW GermanCustomers WITH SCHEMABINDING
AS
	select companyname, contactname, city, phone
	from Sales.Customers
	where country = 'Germany'

	-- adding with schema binding option which materializes the view

GO
ALTER VIEW GermanCustomers WITH SCHEMABINDING
AS
	select companyname, contactname, city, phone, country
	from Sales.Customers
	where country = 'Germany'
	WITH CHECK OPTION -- adding a check option makes sure that when data is updated through the view the update cannot cause the view to show different results bacuse of the where predicate

GO
update GermanCustomers set country = 'UK' -- Error