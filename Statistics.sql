/* Query for dropping the statistics on the columns of a table that are auto generated and are not part of the index */

declare @statisticsName nvarchar(255), @query nvarchar(1000)

declare statistics_cursor cursor for
select name
from sys.stats
where object_id = OBJECT_ID('Sales.Orders', 'U') and auto_created = 1

open statistics_cursor;

fetch next from statistics_cursor into @statisticsName

while @@FETCH_STATUS = 0
begin


	/* DROP STATISTICS STATEMENT */
	set @query = 'drop statistics Sales.Orders.' + @statisticsName + ';';
	
	exec (@query);

	fetch next from statistics_cursor into @statisticsName
end

close statistics_cursor
deallocate statistics_cursor


select *
from sys.stats
where object_id = OBJECT_ID('Sales.Orders', 'U') -- only indexes are left

alter index idx_nc_empid on Sales.Orders REBUILD -- command used to alter the index (rebuild)

/* HSTOGRAM */ 

dbcc show_statistics(N'Sales.Orders', N'idx_nc_empid') with histogram

/* HEADER */
dbcc show_statistics(N'Sales.Orders', N'idx_nc_empid') with stat_header

drop index idx_nc_shipcity on Sales.Orders

/* Statistics auto generated for column shipcity that is used as SARG and is not indexed */
select *
from Sales.Orders
where shipcity = N'Vancouver'

/* Showing auto generated statistics for the table, including the column name/s */

select 
	OBJECT_NAME(st.object_id) as tableName,
	col.name as columnName
from sys.stats st
	 inner join sys.stats_columns stc on st.stats_id = stc.stats_id
	 inner join sys.columns col on col.object_id = st.object_id and col.column_id = stc.column_id
where st.object_id = OBJECT_ID('Sales.Orders', 'U') and auto_created = 1


/* Practice */

alter database tsql2012 set auto_create_statistics off with no_wait;

create nonclustered index idx_nc_custid_shipcity on Sales.Orders(custid, shipcity)

select *
from Sales.Orders
where shipcity = 'Vancouver' -- Clustered index scan - warning: no statistics on column Shipcity (auto_create_statistics is off)

select *
from sys.stats
where object_id = OBJECT_ID('Sales.Orders', 'U') and auto_created = 1 -- no rows returned

create statistics st_shipcity on Sales.Orders(shipcity);
dbcc freeproccache

select *
from Sales.Orders
where shipcity = 'Vancouver' -- No statistics warning this time