declare @x as xml;

set @x=N'
<bookstore>  
  <book category="COOKING">  
    <title lang="en">Everyday Italian</title>  
    <author>Giada De Laurentiis</author>  
    <year>2005</year>  
    <price>30.00</price>  
  </book>  
  <book category="CHILDREN">  
    <title lang="en">Harry Potter</title>  
    <author>J K. Rowling</author>  
    <year>2005</year>  
    <price>29.99</price>  
  </book>  
  <book category="WEB">  
    <title lang="en">Learning XML</title>  
    <author>Erik T. Ray</author>  
    <year>2003</year>  
    <price>39.95</price>  
  </book>  
</bookstore>  
'

select @x.exist(
	'//year'
) -- returns 1

select @x.exist(
	'//test'
) -- returns 0

select @x.value('(//book[@category="WEB"]/title)[1]','nvarchar(255)')

/* 
Learning XML
*/

set @x = NULL

select @x.exist(
	'//year'
) -- returns NULL

GO

/* UPDATING XML DATA */

declare @x as XML;

set @x = '
<row>
  <node>
    <name>Alessandro</name>
  </node>
  <node>
    <name>Francesco</name>
  </node>
  <node>
    <name>Daniele</name>
  </node>
</row>
';

-- 3 options with modify function: 1.insert; 2.delete; 3.replace value of
set @x.modify('
	insert element test { "hehehe" } as last into (row/node)[1]
')

/*
<row>
  <node>
    <name>Alessandro</name>
    <test>1</test>
  </node>
  <node>
    <name>Francesco</name>
  </node>
  <node>
    <name>Daniele</name>
  </node>
</row>
*/

set @x.modify('
	insert <test> { "hehehe2" } </test> as last into (row/node)[2]
')

/*
<row>
  <node>
    <name>Alessandro</name>
    <test>hehehe</test>
  </node>
  <node>
    <name>Francesco</name>
    <test>hehehe2</test>
  </node>
  <node>
    <name>Daniele</name>
  </node>
</row>
*/

set @x.modify('
	replace value of (row/node/name/text())[1] with "Mirza"
') -- value that needs to be replaced must be atomic - text() used to extract the text to be replaced

/*
<row>
  <node>
    <name>Mirza</name>
    <test>hehehe</test>
  </node>
  <node>
    <name>Francesco</name>
    <test>hehehe2</test>
  </node>
  <node>
    <name>Daniele</name>
  </node>
</row>
*/

set @x.modify(
	'delete (row/node/name)[1]'
) -- deleting first name element

/*
<row>
  <node>
    <test>hehehe</test>
  </node>
  <node>
    <name>Francesco</name>
    <test>hehehe2</test>
  </node>
  <node>
    <name>Daniele</name>
  </node>
</row>
*/

GO


/* SHREDING XML WITH NODES METHOD */

DECLARE @x AS XML;

SET @x = N'
<CustomersOrders>
	<Customer custid="1">
		<!-- Comment 111 -->
		<companyname>Customer NRZBB</companyname>
		<Order orderid="10692">
			<orderdate>2007-10-03T00:00:00</orderdate>
		</Order>
		<Order orderid="10702">
			<orderdate>2007-10-13T00:00:00</orderdate>
		</Order>
		<Order orderid="10952">
			<orderdate>2008-03-16T00:00:00</orderdate>
		</Order>
	</Customer>
	<Customer custid="2">
		<!-- Comment 222 -->
		<companyname>Customer MLTDN</companyname>
		<Order orderid="10308">
			<orderdate>2006-09-18T00:00:00</orderdate>
		</Order>
		<Order orderid="10952">
			<orderdate>2008-03-04T00:00:00</orderdate>
		</Order>
	</Customer>
</CustomersOrders>';

select result.name.value('data(orderdate[1])[1]', 'datetime')
from @x.nodes('//Customer[@custid="1"]/Order') as result(name) -- shreding orderdate values for the first customer to datetime SQL type

/*
2007-10-03 00:00:00.000
2007-10-13 00:00:00.000
2008-03-16 00:00:00.000
*/
