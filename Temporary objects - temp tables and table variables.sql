select DATEPART(YY, orderdate), COUNT(*)
from Sales.Orders
group by DATEPART(YY, orderdate) -- can use YEAR(orderdate) instead


;with cte as 
(
	select DATEPART(YY, orderdate) as sales_year, COUNT(*) sales_count
	from Sales.Orders
	group by DATEPART(YY, orderdate) -- can use YEAR(orderdate) instead
)
select previousSales.sales_year, previousSales.sales_count, currentSales.sales_year, currentSales.sales_count, previousSales.sales_count - currentSales.sales_count as [difference]
from cte as previousSales
	 inner join cte as currentSales on currentSales.sales_year = previousSales.sales_year + 1 -- Execution plan shows that grouping and aggregating was done 2 times

/* Same query but instead of CTE, table variable is used to prevent multiple scans */

declare @saleCounts table (sales_year int not null, sales_count int not null);

insert into @saleCounts (sales_year, sales_count)
select DATEPART(YY, orderdate) as sales_year, COUNT(*) sales_count
from Sales.Orders
group by DATEPART(YY, orderdate) -- can use YEAR(orderdate) instead

select previousSales.sales_year, previousSales.sales_count, currentSales.sales_year, currentSales.sales_count, previousSales.sales_count - currentSales.sales_count as [difference]
from @saleCounts as previousSales
	 inner join @saleCounts as currentSales on currentSales.sales_year = previousSales.sales_year + 1 -- scanning, grouping and aggregating done only once







