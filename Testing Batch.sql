﻿select c.companyname, MAX(o.orderdate) as MostRecentOrderDate
from Sales.Customers c
	 inner join Sales.Orders o on c.custid = o.custid
group by c.companyname
order by MostRecentOrderDate

--You need to create a query that returns a list of products from Sales.ProductCatalog. The solution must meet the following requirements:
---> Return rows ordered by descending values in the UnitPrice column.
---> Use the Rank function to calculate the results based on the UnitPrice column.
---> Return the ranking of rows in a column that uses the alias PriceRank.
---> Use two-part names to reference tables.
---> Display the columns in the order that they are defined in the table. The PriceRank column must appear last.
--Part of the correct T-SQL statement has been provided in the answer area. Provide the complete code.

select productid, unitprice, RANK() OVER(order by UnitPrice desc) as PriceRank
from Sales.OrderDetails
order by PriceRank

select productid, unitprice, RANK(ORDER BY UnitPrice DESC) OVER () as PriceRank
from Sales.OrderDetails
order by PriceRank

SELECT CatID, CatName, ProductID, ProdName, UnitPrice, RANK (ORDER BY UnitPrice DESC) OVER () AS PriceRank 
FROM Sales.ProductCatalog 
ORDER BY PriceRank

select * from Sales.OrderDetails

GO

CREATE FUNCTION dbo.OrdersWithNoSalesPerson(@ProductID INT)
RETURNS table
AS
RETURN
select * from Sales.OrderDetails

GO
CREATE FUNCTION [dbo].[FunctionName]
(
    @param1 int
)
RETURNS TABLE AS RETURN
SELECT @param1 AS c1


CREATE FUNCTION [dbo].[FunctionName]
(
    @param1 int,
    @param2 char(5)
)
RETURNS @returntable TABLE 
(
	[c1] int,
	[c2] char(5)
)
AS
BEGIN
    INSERT @returntable
    SELECT @param1, @param2
    RETURN 
END

GO
WITH CTEDupRecords AS ( 
	SELECT MIN(supplierid) AS CreatedDateTime, productname 
	FROM Production.Products 
	GROUP BY productname
	HAVING COUNT(*) > 1 
) 
select *
from Production.Products p
	 inner join CTEDupRecords dup on p.productid = dup.productid and p.unitprice > dup.CreatedDateTime


SELECT c.companyname 
FROM Sales.Customers c 
	 INNER JOIN Sales.Orders s ON c.custid = s.custid
GROUP BY c.custid
HAVING MAX(s.orderdate) < DATEADD(DAY, -90, GETDATE())

declare @currentDate datetime = '2008-05-06 00:00:00.000'

SELECT c.companyname 
FROM Sales.Customers c 
WHERE NOT EXISTS ( 
	SELECT s.OrderDate 
	FROM Sales.Orders s 
	WHERE s.OrderDate > DATEADD(DAY, -90, @currentDate) AND s.custid = c.custid
)

select * from Sales."Orders"


GO

create table OrderDetails (
	ListPrice money,
	Quantity int
)

create table Customers (
	CustomerID int identity(1,1),
	FirstName nvarchar(255),
	LastName nvarchar(255)
)

alter table Customers add constraint PK_Customers primary key (CustomerID)

create table Orders (
	OrderID int identity(1,1),
	OrderData datetime,
	CustomerID int foreign key references Customers (CustomerId)
)


GO
--✑ Return the most recent orders first.
--✑ Use the first initial of the table as an alias.
--✑ Return the most recent order date for each customer.
--✑ Retrieve the last name of the person who placed the order.
--✑ Return the order date in a column named MostRecentOrderDate that appears as the last column in the report.

select c."LastName", MAX(o."OrderData") as "MostRecentOrderDate"
from "Customers" as c
	 inner join "Orders" o 
		on o."CustomerID" = c."CustomerID"
group by c."CustomerID", c."LastName"
order by MostRecentOrderDate



--You need to recreate the query to meet the following requirements:
--✑ Reference columns by using one-part names only.
--✑ Sort aggregates by SalesTerritoryID, and then by ProductID.
--✑ Order the results in descending order from SalesTerritoryID to ProductID.
--✑ The solution must use the existing SELECT clause and FROM clause.

select 
	productid,
	AVG(unitprice),
	MAX(qty),
	MAX(discount)
from Sales.OrderDetails
group by SalesTeritoryID, ProductID
order by SalesTeritoryID desc, ProductID desc

GO

--✑ UnitPrice must be returned in descending order.
--✑ The query must use two-part names to reference the table.
--✑ The query must use the RANK function to calculate the results.
--✑ The query must return the ranking of rows in a column named PriceRank.
--✑ The list must display the columns in the order that they are defined in the table.

select ProductCatalog.CatID, ProductCatalog.CatName, ProductCatalog.ProductID, ProductCatalog.ProdName, ProductCatalog.UnitPrice, RANK() OVER(order by UnitPrice desc) as PriceRank
from Sales.ProductCatalog
order by ProductCatalog.UnitPrice desc

GO
--You have an application named Appl. You have a parameter named @Count that uses the int data type. App1 is configured to pass @Count to a stored procedure. 
--You need to create a stored procedure named usp_Customers for Appl. Usp_Customers must meet the following requirements:
--✑ NOT use object delimiters.
--✑ Minimize sorting and counting.
--✑ Return only the last name of each customer in alphabetical order.
--✑ Return only the number of rows specified by the @Count parameter.
--✑ The solution must NOT use BEGIN and END statements.

create procedure usp_Customers 
	@Count int
as
	select top (@Count) Customers.companyname
	from Sales.Customers
	order by Customers.companyname asc


-- You need to view the country from which each customer has placed the most orders.
SELECT 
	c.custid, c.companyname, o.shipcountry 
FROM Sales.Customers c 
	 INNER JOIN (
		SELECT custid, shipcountry, RANK() OVER (PARTITION BY custid ORDER BY COUNT(freight) DESC) AS Rnk 
		FROM Sales.Orders 
		GROUP BY custid, shipcountry
) AS o ON c.custid = o.custid WHERE o.Rnk = 1

GO

SET XACT_ABORT ON;

GO
create procedure deleteInactiveCustomers 
as
BEGIN
	delete c
	from Sales.Customers c 
	where not exists (select 1 from Sales.Orders where custid = c.custid);

	THROW 50000, 'Error in usp_InsertCategories stored procedure', 0;	
END

GRANT EXECUTE ON deleteInactiveCustomers TO testDeleteUser

--You have a stored procedure named Procedure1. Procedure1 retrieves all order ids after a specific date. The rows for Procedure1 are not sorted. 
--Procedure1 has a single parameter named Parameter1. Parameter1 uses the varchar type and is configured to pass the specific date to Procedure1. 
--A database administrator discovers that OrderDate is not being compared correctly to Parameter1 after the data type of the column is changed to datetime. You need to update the

--SELECT statement to meet the following requirements:
--✑ The code must NOT use aliases.
--✑ The code must NOT use object delimiters.
--✑ The objects called in Procedure1 must be able to be resolved by all users.
--✑ OrderDate must be compared to Parameter1 after the data type of Parameter1 is changed to datetime.

declare @Parameter1 varchar(255) = '2007-07-04 00:00:00.000'

select Orders.orderid
from Sales.Orders
where Orders.orderdate > @Parameter1

GO
CREATE VIEW Sales.uv_CustomerFullNames WITH SCHEMABINDING
AS
SELECT companyname
FROM Sales.Customers

GO
CREATE TABLE OrderDetailsTEST (
	ListPrice decimal(18,2),
	Quantity int,
	LineItemTotal AS ListPrice * Quantity PERSISTED
)

select orderid, SUM(unitprice) as TotalSales
from Sales.OrderDetails
group by orderid
order by orderid asc

GO
CREATE FUNCTION Sales.fn_OrderByTeritory (@SalesTerritoryID int)
RETURNS TABLE ASRETURN
(
	SELECT OrderID, OrderDate
	FROM Sales.Orders
	WHERE orderid = @SalesTerritoryID
)

GO
CREATE FUNCTION Sales.fn_TestMultiline (@testParam int)
RETURNS @testTable TABLE (
	id int
)
AS
BEGIN
	INSERT INTO @testTable select 1;

	RETURN;
END

GO
SELECT OrderDates (OrdeDate)
AS (

)


GO

SELECT 
	Products.categoryid, 
	Products.ProductID, 
	Products.productname, 
	RANK() OVER(ORDER BY Products.UnitPrice) AS PriceRank
FROM Production.Products
ORDER BY Products.UnitPrice DESC


SELECT MAX(C.companyname) as LastName, MAX(O.OrderDate) AS MostRecentOrder
FROM Sales.Customers as C
	 INNER JOIN Sales.Orders as O ON C.custid = O.custid
GROUP BY C.custid
ORDER BY MostRecentOrder


SELECT custid, companyname, shipcountry 
FROM (
	SELECT c.custid, c.companyname, o. shipcountry, RANK() OVER (PARTITION BY c.custid ORDER BY o.freight DESC) AS Rnk 
	FROM Sales.Customers c 
		 INNER JOIN Sales.Orders o ON c.custid = o.custid 
	GROUP BY c.custid, c.companyname, o.shipcountry
) cs 
WHERE Rnk = 1

SELECT c.custid, c.companyname, o.shipcountry
FROM Sales.Customers c 
	 INNER JOIN (
		SELECT custid, shipcountry, RANK() OVER (PARTITION BY custid ORDER BY COUNT(freight) DESC) AS Rnk 
		FROM Sales.Orders 
		GROUP BY custid, shipcountry
	 ) AS o ON c.custid = o.custid

GO

