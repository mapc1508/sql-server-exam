-- sequential vs non-sequential

/* SEQUENTIAL */

-- IDENTITY
create table testIdentity 
(
	id int identity(1,1), -- start with 1, increment by 1
	name nvarchar(255)
)

-- SEQUENCE OBJECT

create sequence testSequenceObject
start with 1 
increment by 1
minvalue 1
maxvalue 100
cycle

-- NEWSEQUENTIALID

create table testNEWSEQUENTIALID
(
	id uniqueidentifier default(NEWSEQUENTIALID()),
	name nvarchar(255)
)

insert into testNEWSEQUENTIALID (name) values ('TEST')
insert into testNEWSEQUENTIALID (name) values ('TEST1')
select * from testNEWSEQUENTIALID

--7229C9BD-E69D-E911-AB72-54EE75F15477
--6038ADCA-E69D-E911-AB72-54EE75F15477

/* NON-SEQUENTIAL */

create table testNONSEQUENTIALID
(
	id uniqueidentifier default(NEWID()),
	name nvarchar(255)
)

insert into testNONSEQUENTIALID (name) values ('TEST')
insert into testNONSEQUENTIALID (name) values ('TEST1')
select * from testNONSEQUENTIALID

--8BD91104-17CA-440F-9949-C6C2E2A016BD
--8DFCC036-E4BD-4E68-B8B3-5D5A0F36F58D