/* CREATING A SEQUNCE OBJECT */
create sequence testSequence
start with 1
increment by 1;
GO

/* GETING NEXT VALUE */
select next value for testSequence;

/* USING SEQUNCE OBJECT IN A QUERY */
select Customers.contactname, next value for testSequence over(order by custid)
from Sales.Customers
where custid < 11

/* CREATING CYCLIC SEQUENCE */
create sequence testCyclicSequence
start with 1
increment by 1
minvalue 1
maxvalue 100
cycle

select Customers.contactname, next value for testCyclicSequence over(order by custid)
from Sales.Customers
where custid < 11

/* resetting the sequence */

alter sequence testSequence 
restart with 1;

/* using sequnce in default constraint */

create table testTableSequence 
(
	id int not null default(next value for testSequence), -- adding default value that selects next value from sequence object
	name nvarchar(255)
)

/* caching sequence values */

alter sequence testCyclicSequence
cache 100; -- caching will make sure that only after 100 generated sequence numbers the server will write to diske
	       -- current value and number of values left are saved in memory

/* Getting info about all sequence objects */

select * from sys.sequences

/* Generating range of sequence values at once */

declare @firstValue sql_variant
exec sp_sequence_get_range N'testSequence', 50, @firstValue OUT
select @firstValue -- 51 - first value after sequence generated 50 values

