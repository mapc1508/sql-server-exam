-- we can explicitly say, for example, to use scan instead of seek - need to be very careful
	-- sql server is pretty good at working out what the rigth method of getting data is
--this is done by using hints
	-- can be at select level, join level etc...

begin tran
update tblEmployee
set Department = 'HR'
where EmployeeNumber = 130
-- transaction has started in another session


select *
from tblDepartment d
	 left join tblEmployee e on d.Department = e.Department
where d.Department = 'HR' 
-- cannot read data while another transaction is updating
-- can use hints

select *
from tblDepartment d
	 left join tblEmployee e with (nolock) on d.Department = e.Department  -- same as read uncommited, reads that is updating in another session
where d.Department = 'HR' 
-- don't need to put the isolation level on the whole database

select *
from tblDepartment d
	 left join tblEmployee e with (repeatableread) on d.Department = e.Department  -- same but different isolation level
where d.Department = 'HR' 

select *
from tblDepartment d
	 left join tblEmployee e with (repeatableread, forceseek) on d.Department = e.Department -- can use multiple hints separated by comma
where d.Department = 'HR' 
-- this one fails to execute: Query processor could not produce a query plan because of the hints defined in this query. Resubmit the query without specifying any hints and without using SET FORCEPLAN.
-- use hints only when necessary

select *
from tblDepartment d
	 left hash join tblEmployee e on d.Department = e.Department -- join hint - hash/merge/loop (put after the type of join)
where d.Department = 'HR' 
-- can be useful to compare how different joins would work under the same query

select *
from tblDepartment d
	 left loop join tblEmployee e on d.Department = e.Department
where d.Department = 'HR' -- 19%

select *
from tblDepartment d
	 left hash join tblEmployee e on d.Department = e.Department
where d.Department = 'HR' -- 43%

select *
from tblDepartment d
	 left merge join tblEmployee e on d.Department = e.Department
where d.Department = 'HR' -- 39%


	 

