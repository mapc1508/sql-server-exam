/* Batch processing and columnstore indexes - Data warehousing scenario */

DECLARE
@dim1rows AS INT = 100,
@dim2rows AS INT = 50,
@dim3rows AS INT = 200;
-- First dimension
CREATE TABLE dbo.Dim1
(
key1 INT NOT NULL CONSTRAINT PK_Dim1 PRIMARY KEY,
attr1 INT NOT NULL,
filler BINARY(100) NOT NULL DEFAULT (0x)
);
-- Second dimension
CREATE TABLE dbo.Dim2
(
key2 INT NOT NULL CONSTRAINT PK_Dim2 PRIMARY KEY,
attr1 INT NOT NULL,
filler BINARY(100) NOT NULL DEFAULT (0x)
);
-- Third dimension
CREATE TABLE dbo.Dim3
(
key3 INT NOT NULL CONSTRAINT PK_Dim3 PRIMARY KEY,
attr1 INT NOT NULL,
filler BINARY(100) NOT NULL DEFAULT (0x)
);
-- Fact table
CREATE TABLE dbo.Fact
(
key1 INT NOT NULL CONSTRAINT FK_Fact_Dim1 FOREIGN KEY REFERENCES dbo.Dim1,
key2 INT NOT NULL CONSTRAINT FK_Fact_Dim2 FOREIGN KEY REFERENCES dbo.Dim2,
key3 INT NOT NULL CONSTRAINT FK_Fact_Dim3 FOREIGN KEY REFERENCES dbo.Dim3,
measure1 INT NOT NULL,
measure2 INT NOT NULL,
measure3 INT NOT NULL,
filler BINARY(100) NOT NULL DEFAULT (0x),
CONSTRAINT PK_Fact PRIMARY KEY(key1, key2, key3)
);

-- Populating the first dimension
INSERT INTO dbo.Dim1(key1, attr1)
SELECT n, ABS(CHECKSUM(NEWID())) % 20 + 1
FROM dbo.GetNums(1, @dim1rows);
-- Populating the second dimension
INSERT INTO dbo.Dim2(key2, attr1)
SELECT n, ABS(CHECKSUM(NEWID())) % 10 + 1
FROM dbo.GetNums(1, @dim2rows);
-- Populating the third dimension
INSERT INTO dbo.Dim3(key3, attr1)
SELECT n, ABS(CHECKSUM(NEWID())) % 40 + 1
FROM dbo.GetNums(1, @dim3rows);
-- Populating the fact table
INSERT INTO dbo.Fact WITH (TABLOCK)
(key1, key2, key3, measure1, measure2, measure3)
SELECT N1.n, N2.n, N3.n,
ABS(CHECKSUM(NEWID())) % 1000000 + 1,
ABS(CHECKSUM(NEWID())) % 1000000 + 1,
ABS(CHECKSUM(NEWID())) % 1000000 + 1
FROM dbo.GetNums(1, @dim1rows) AS N1
CROSS JOIN dbo.GetNums(1, @dim2rows) AS N2
CROSS JOIN dbo.GetNums(1, @dim3rows) AS N3;




-- Measuring IO and time
SET STATISTICS IO ON;
SET STATISTICS TIME ON;
-- Query demonstrating star join
SELECT 
	D1.attr1 AS x, D2.attr1 AS y, D3.attr1 AS z,
	COUNT(*) AS cnt, SUM(F.measure1) AS total
FROM dbo.Fact AS F
	 INNER JOIN dbo.Dim1 AS D1 ON F.key1 = D1.key1
	 INNER JOIN dbo.Dim2 AS D2 ON F.key2 = D2.key2
	 INNER JOIN dbo.Dim3 AS D3 ON F.key3 = D3.key3
WHERE 
	D1.attr1 <= 10
	AND D2.attr1 <= 15
	AND D3.attr1 <= 10
GROUP BY 
	D1.attr1, D2.attr1, D3.attr1;

--SQL Server Execution Times:
--   CPU time = 233 ms,  elapsed time = 252 ms.

--Table 'Fact'. Scan count 55, logical reads 9501, physical reads 0, read-ahead reads 0, lob logical reads 0, lob physical reads 0, lob read-ahead reads 0

CREATE COLUMNSTORE INDEX idx_cs_fact ON dbo.Fact(key1, key2, key3, measure1, measure2, measure3);

-- Measuring IO and time
SET STATISTICS IO ON;
SET STATISTICS TIME ON;
-- Query demonstrating star join
SELECT 
	D1.attr1 AS x, D2.attr1 AS y, D3.attr1 AS z,
	COUNT(*) AS cnt, SUM(F.measure1) AS total
FROM dbo.Fact AS F
	 INNER JOIN dbo.Dim1 AS D1 ON F.key1 = D1.key1
	 INNER JOIN dbo.Dim2 AS D2 ON F.key2 = D2.key2
	 INNER JOIN dbo.Dim3 AS D3 ON F.key3 = D3.key3
WHERE 
	D1.attr1 <= 10
	AND D2.attr1 <= 15
	AND D3.attr1 <= 10
GROUP BY 
	D1.attr1, D2.attr1, D3.attr1;


--With columnstore index in place the burden on CPU is much lower since it operates on a batch of rows
--CPU time spent is also significantly lower

 --SQL Server Execution Times:
 --  CPU time = 15 ms,  elapsed time = 251 ms.
 

