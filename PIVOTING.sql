-- want to know total freight for each shipper in countries: Germany, USA and France
-- result needs to be in format: Shipper, Germany, USA, France

-- best is to prepare 3 required elements in table expression before using pivot statement (not on directly on the table)
with pivotData as 
(
	select 
		shipperid, -- goruping element
		shipcountry, -- spreading element
		freight -- aggregation element
	from Sales.Orders
)
select 
	shipperid, p.Germany, p.USA, p.France
from
	pivotData PIVOT(
		SUM(freight) -- aggregation function + element; cannot have multiple aggregate functions at once
		FOR shipcountry in (Germany, USA, France) -- this cannot be result of computation or expression
	)  as p
order by shipperid

-- RESULT
--1	3671.96	2294.72	1149.98
--2	3964.33	5778.58	1264.49
--3	3646.99	5697.99	1823.37

-- if want to use COUNT(*) aggregate function, need to specify the column as a parameter, cannot use COUNT(*) form
-- can specify dummy aggregation column instead
with pivotData as 
(
	select 
		shipperid, -- goruping element
		shipcountry, -- spreading element
		1 as aggr_col -- aggregation element - using dummy aggregation element
	from Sales.Orders
)
select 
	shipperid, p.Germany, p.USA, p.France
from
	pivotData PIVOT(
		COUNT(aggr_col) -- aggregation function + element
		FOR shipcountry in (Germany, USA, France)
	)  as p
order by shipperid

-- RESULT
--1	41	31	27
--2	53	51	29
--3	28	40	21

/* UNPIVOTING */

-- taking pivoted data and rotating it - result columns: grouping column, spreading column, aggregation column

declare @pivotedData table (shipperid int, Germany nvarchar(255), USA nvarchar(255), France nvarchar(255)); -- temp table where pivoted data is stored

with pivotData as 
(
	select 
		shipperid, -- goruping element
		shipcountry, -- spreading element
		freight -- aggregation element
	from Sales.Orders
)
insert into @pivotedData(shipperid, Germany, USA, France)
select 
	shipperid, p.Germany, p.USA, p.France
from
	pivotData PIVOT(
		SUM(freight) 
		FOR shipcountry in (Germany, USA, France)
	)  as p
order by shipperid

select 
	shipperid, -- source column - don't need to specify this name, it exists from the pivoted table
	u.freight, 
	u.shipcountry
from 
	@pivotedData UNPIVOT(
		freight -- target value column (what used to be aggregation element) -- need to specify name
		for shipcountry in (Germany, USA, France) -- target name column (what used to be spreading element) -- need to specify name
	) as u

	



