select 
	ROW_NUMBER() OVER(order by orderdate), 
	RANK() OVER(order by orderdate), 
	DENSE_RANK() OVER(order by orderdate), 
	orderid,
	orderdate
from Sales.Orders
order by orderdate

--RESULT

--1	 1	1	10248	2006-07-04 00:00:00.000
--2	 2	2	10249	2006-07-05 00:00:00.000
--3	 3	3	10250	2006-07-08 00:00:00.000 
--4	 3	3	10251	2006-07-08 00:00:00.000 -- when two rows have same value ROW_NUMBER() arbitrarily assigns consecutive numbers, RANK() and DENSE_RANK() assign the same number
--5	 5	4	10252	2006-07-09 00:00:00.000 -- differnce between RANK and DENSE_RANK is the next value after consecutive same values, RANK - number of previous rows + 1, DENSE_RANK - num of previous distinct rows + 1 (no gaps)

/* NTILE */

select custid, NTILE(10) OVER(order by custid)
from Sales.Customers -- total:92; one tile = 92/10 = (9 * 10) + 2 = first two rows have tiles of 9 (standard) + 1 (remainder) rows, others have 9 (standard) rows