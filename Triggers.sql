GO
CREATE TRIGGER Production.tr_ProductsUpdates on Production.Products
AFTER DELETE, INSERT, UPDATE
AS
BEGIN
	if @@ROWCOUNT = 0 return;

	set nocount on;

	/* JUST FOR TESTING! Should not return result set from the trigger */
	select COUNT(*) as count_inserted from inserted;
	select COUNT(*) as count_deleted from deleted;
END


update p set p.unitprice = 19.5 from Production.Products p where productid = 1 -- count_inserted: 1; count_deleted: 1

drop trigger Production.tr_ProductsUpdates


/* AFTER TRIGGER */
go
create trigger Production.tr_CheckProductDuplicates on Production.Products
after insert, update
as
begin
	if @@ROWCOUNT = 0 return;

	set nocount on;

	-- trigger checks duplicates and throws error if found
	if exists (
		select p.productname
		from Production.Products p
			 inner join inserted on p.productname = inserted.productname
		group by p.productname
		having COUNT(*) > 1
	)
	begin
		;throw 50000, 'Duplicate product names are not allowed', 0; 
	end
end

insert into Production.Products
select 'Product HHYDP1', 1, 1, 15, 0, NULL

insert into Production.Products
select 'Product HHYDP1', 1, 1, 20, 0, NULL -- trigger throws error

drop trigger Production.tr_CheckProductDuplicates


/* INSTEAD OF TRIGGER */
go
create trigger Production.tr_CheckProductDuplicates_InsteadOf on Production.Products
instead of insert, update
as
begin
	if @@ROWCOUNT = 0 return;

	set nocount on;

	-- trigger checks duplicates and throws error if found
	if exists (
		select p.productname
		from Production.Products p
			 inner join inserted on p.productname = inserted.productname
		group by p.productname
		having COUNT(*) > 1
	)
	begin
		;throw 50000, 'Duplicate product names are not allowed', 0; 
	end
	else
	begin
		insert into Production.Products (productname, supplierid, categoryid, unitprice, discontinued, additionalattributes)
		select productname, supplierid, categoryid, unitprice, discontinued, additionalattributes
		from inserted 
	end
end
go

insert into Production.Products
select 'Product HHYDP1123', 1, 1, 15, 0, NULL

insert into Production.Products
select 'Product HHYDP1123', 1, 1, 20, 0, NULL -- trigger throws error

